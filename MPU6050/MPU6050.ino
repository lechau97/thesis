#include "MPU6050.h"

//#include "MPU6050.H"
#define MPU6050_ADDRESS_HIGH
char str[50];
int16_t AccX, AccY, AccZ, GyrX, GyrY, GyrZ, Temp;
int8_t AccXH, AccXL, AccYH, AccYL, AccZH, AccZL, GyrXH, GyrXL, GyrYH, GyrYL, GyrZH, GyrZL, TmpH, TmpL;
void setup()
{
	Wire.begin();
	MPU6050_Int();
	Serial.begin(115200);
}

void loop()
{
	AccXH = MPU6050_Read(MPU6050_RA_ACCEL_XOUT_L);
	AccXL = MPU6050_Read(MPU6050_RA_ACCEL_XOUT_H);
	AccYH = MPU6050_Read(MPU6050_RA_ACCEL_YOUT_H);
	AccYL = MPU6050_Read(MPU6050_RA_ACCEL_YOUT_L);
	AccZH = MPU6050_Read(MPU6050_RA_ACCEL_ZOUT_H);
	AccZL = MPU6050_Read(MPU6050_RA_ACCEL_ZOUT_L);

	TmpH = MPU6050_Read(MPU6050_RA_TEMP_OUT_H);
	TmpL = MPU6050_Read(MPU6050_RA_TEMP_OUT_L);

	GyrXH = MPU6050_Read(MPU6050_RA_GYRO_XOUT_H);
	GyrXL = MPU6050_Read(MPU6050_RA_GYRO_XOUT_L);
	GyrYH = MPU6050_Read(MPU6050_RA_GYRO_YOUT_H);
	GyrYL = MPU6050_Read(MPU6050_RA_GYRO_YOUT_L);
	GyrZH = MPU6050_Read(MPU6050_RA_GYRO_ZOUT_H);
	GyrZL = MPU6050_Read(MPU6050_RA_GYRO_ZOUT_L);

	// Serial.println(MPU6050_Read(MPU6050_RA_WHO_AM_I));

	Serial.println(&AccXH);
	AccX = AccXH << 8 | AccXL;
	AccY = AccYH << 8 | AccYL;
	AccZ = AccZH << 8 | AccZL;

	Temp = TmpH << 8 | TmpL;

	GyrX = GyrXH << 8 | GyrXL;
	GyrY = GyrYH << 8 | GyrYL;
	GyrZ = GyrZH << 8 | GyrZL;

	Serial.print("AccX = "); Serial.print(AccX);
	Serial.print(" | AccY = "); Serial.print(AccY);
	Serial.print(" | AccZ = "); Serial.print(AccZ);

	Serial.print(" | Tmp = "); Serial.print(Temp/340.00+36.53);

	Serial.print(" | GyrX = "); Serial.print(GyrX);
	Serial.print(" | GyrY = "); Serial.print(GyrY);
	Serial.print(" | GyrZ = "); Serial.println(GyrZ);
	sprintf(str, "%d\t|%d\t|%d\t|%d\t|%d\t|%d\t|%d", AccX, AccY, AccZ, Temp, GyrX, GyrY, GyrZ);
	Serial.println(str);
	delay(400);
}
