
#include "MPU6050.h"

void MPU6050_Write(byte address, byte data)
{
	Wire.beginTransmission(MPU6050_ADDRESS);
	Wire.write(address);
	Wire.write(data);
	Wire.endTransmission();
}

byte MPU6050_Read(byte address)
{
	byte data;
	Wire.beginTransmission(MPU6050_ADDRESS);
	//Serial.println("1.");
	Wire.write(address);
	//Serial.println("2.");
	Wire.endTransmission(false);
	//Serial.println("3.");
	Wire.requestFrom(MPU6050_ADDRESS, (byte)1);
	//Serial.println("4.");
	data = Wire.read(); // receive a byte as character
	Wire.endTransmission();
	return data;
}

void MPU6050_Int()
{

	MPU6050_Write(MPU6050_RA_PWR_MGMT_1, 0x01);
	delay(200);
	MPU6050_Write(MPU6050_RA_CONFIG, 0x03);
	delay(200);
	MPU6050_Write(MPU6050_RA_SMPLRT_DIV, 0x04);
	delay(200);
	byte c = MPU6050_Read(MPU6050_RA_GYRO_CONFIG);
	MPU6050_Write(MPU6050_RA_GYRO_CONFIG, c & ~0xE0);
	MPU6050_Write(MPU6050_RA_GYRO_CONFIG, c & ~0x18);
	MPU6050_Write(MPU6050_RA_GYRO_CONFIG, c);
	delay(200);
	c = MPU6050_Read(MPU6050_RA_ACCEL_CONFIG);
	MPU6050_Write(MPU6050_RA_ACCEL_CONFIG, c & ~0xE0);
	MPU6050_Write(MPU6050_RA_ACCEL_CONFIG, c & ~0x18);
	MPU6050_Write(MPU6050_RA_ACCEL_CONFIG, c);

	delay(200);
	// MPU6050_Write(MPU6050_RA_USER_CTRL,0x00);
	// delay(200);
}
