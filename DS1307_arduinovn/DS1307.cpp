#include "DS1307.h"

int bcd2dec(byte num)
{
    return num / 16 * 10 + num % 16;
}

int dec2bcd(byte num)
{
    return num / 10 * 16 + num % 10;
}

void setTime(int Add, int num, int hr, int minu, int sec, int wd, int d, int mth, int yr){
    Wire.beginTransmission(Add);
    Wire.write(byte(0x00));
    Wire.write(dec2bcd(sec));
    Wire.write(dec2bcd(minu));
    Wire.write(dec2bcd(hr));
    Wire.write(dec2bcd(wd)); // day of week: Sunday = 1, Saturday = 7
    Wire.write(dec2bcd(d));
    Wire.write(dec2bcd(mth));
    Wire.write(dec2bcd(yr));
    Wire.endTransmission();
}

void DS1307_Read(int add, int num, int *hour, int *minute, int *second, int *wday,int *day, int *month, int *year){
    Wire.beginTransmission(add);
    Wire.write((byte)0x00);
    Wire.endTransmission();
    Wire.requestFrom(add, num);
    second = bcd2dec(Wire.read() & 0xff);
    minute = bcd2dec(Wire.read());
    // hour = bcd2dec(Wire.read() & 0x3f);     //24h
    hour = bcd2dec(Wire.read());            //12h
    wday = bcd2dec(Wire.read());
    month = bcd2dec(Wire.read());
    year = bcd2dec(Wire.read());
    year += 2000;
}