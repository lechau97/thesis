#include "DS1307.h"

const byte DS1307_Address = 0x68;

const byte numberOfFields = 7;

int second, minute, hour, day, wday, month, year;

void digitalClockDisplay();
void printDigits(int digits);

void setup()
{
    Wire.begin();
    setTime(DS1307_Address, numberOfFields ,1, 1, 1, 1, 1, 1, 1);
    Serial.begin(115200);
}

void loop()
{
    DS1307_Read(DS1307_Address, numberOfFields, &hour, &minute, &second, &wday, &day, &month, &year);
    digitalClockDisplay();
    delay(500);
}

void digitalClockDisplay()
{
    Serial.print(hour);
    printDigits(minute);
    printDigits(second);
    Serial.print(" ");
    Serial.print(day);
    Serial.print(" ");
    Serial.print(month);
    Serial.print(" ");
    Serial.print(year);
    Serial.println();
}

void printDigits(int digits)
{
    Serial.print(":");
    if (digits < 10)
        Serial.print('0');
    Serial.print(digits);
}
