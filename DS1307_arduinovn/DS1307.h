#pragma once

#include <Wire.h>
#include <stdint.h>

#define byte uint8_t

int bcd2dec(byte num);
int dec2bcd(byte num);
void setTime(int add, int num, int hr, int minu, int sec, int wd, int d, int mth, int yr);
void DS1307_Read(int add, int num, int *hour, int *minute, int *second, int *wday,int *day, int *month, int *year);