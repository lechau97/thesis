#include <SPI.h>
#include "LoRa.h"


struct MPU6050_Info {
	byte AccXH, AccXL;
	byte AccYH, AccYL;
	byte AccZH, AccZL;
	byte GyrXH, GyrXL;
	byte GyrYH, GyrYL;
	byte GyrZH, GyrZL;
	byte TmpH, TmpL;
};

struct MyPacket
{
	word seq_num;
	MPU6050_Info info;
	uint32_t timestamp;
	float flat, flon;
} my_packet;

void setup()
{
    Serial.begin(9600);
    while (!Serial)
        ;

    Serial.println("LoRa Receiver");

    if (!LoRa.begin(433E6))
    {
        Serial.println("Starting LoRa failed!");
        while (1)
            ;
    }
 


}

void loop()
{
    
    // try to parse packet
    int packetSize = LoRa.parsePacket(sizeof(my_packet));
    if (packetSize == sizeof(my_packet))
    {
        // received a packet
        //Serial.print("Received packet '");

        // read packet
        byte * my_packet_raw = (byte*) &my_packet;
        for(byte i = 0; i < sizeof(my_packet); i++)
        {
            my_packet_raw[i] = LoRa.read();
        }

		Serial.print("AccX: ");
        Serial.println(my_packet.info.AccXH << 8 | my_packet.info.AccXL);
		Serial.print("AccY: ");
		Serial.println(my_packet.info.AccYH << 8 | my_packet.info.AccYL);
		Serial.print("AccZ: ");
		Serial.println(my_packet.info.AccZH << 8 | my_packet.info.AccZL);


        DateTime now = new DateTime(my_packet.timestamp);
        
        Serial.print(now.hour());
        Serial.print(':');
        Serial.print(now.minute());
        Serial.print(':');
        Serial.print(now.second());
        Serial.print(' ');
        Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
        Serial.print(", ");
        Serial.print(now.day());
        Serial.print('/');
        Serial.print(now.month());
        Serial.print('/');
        Serial.println(now.year());

        Serial.print("flat = ");
        Serial.println(my_packet.flat);
        Serial.print("flon = ");
        Serial.println(my_packet.flon);

        //data.write("\n");
        //data.close();

    }
}
