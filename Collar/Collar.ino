#include <SPI.h>
#include <nRF24L01.h>
#include <printf.h>
#include <RF24.h>
#include <RF24_config.h>
#include "AT24C512C.h"
#include <SoftwareSerial.h>
#include <math.h>

#include "RTClib.h"
#include "TinyGPS.h"
#include "LoRa.h"

#define MPU6050_ADDRESS MPU6050_ADDRESS_AD0_HIGH
#include "MPU6050_CONTROLLER.H"

#define RF24_ADDRESS 0xE8E8F0F0E1LL // Address to transfer
#include "RF24_CONTROLLER.H"

struct ReceivePacket
{
	word seq_num;
	MPU6050_Info info;
} receive_packet;

struct MyPacket
{
	word seq_num;
	MPU6050_Info info;
	uint32_t timestamp;
	float flat, flon;
} my_packet;

byte status;
AT24C512C EEPROM;

SoftwareSerial my_Serial(1, 3);

RTC_DS1307 rtc;

TinyGPS gps;

void Serial_Init()
{
	my_Serial.begin(9600);
}

void timer_Init()
{
	cli(); //disable interrupt
	/* Reset Timer/Counter1 */
	TCCR1A = 0;
	TCCR1B = 0;
	TIMSK1 = 0;

	/* Setup Timer/Counter1 */
	TCCR1B |= (1 << CS11) | (1 << CS10); // prescale = 64
	TCNT1 = 59286;
	TIMSK1 = (1 << TOIE1); // Overflow interrupt enable
	sei();								 //Enable interrupt
}

static void smartdelay(unsigned long ms)
{
	unsigned long start = millis();
	do
	{
		while (Serial2.available())
			gps.encode(Serial2.read());
	} while (millis() - start < ms);
}

static void print_float(float val, float invalid, int len, int prec)
{
	if (val == invalid)
	{
		while (len-- > 1)
			Serial.print('*');
		Serial.print(' ');
	}
	else
	{
		Serial.print(val, prec);
		int vi = abs((int)val);
		int flen = prec + (val < 0.0 ? 2 : 1); // . and -
		flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
		for (int i = flen; i < len; ++i)
			Serial.print(' ');
	}
	smartdelay(0);
}

void setup()
{
	Serial.begin(9600);
	Serial2.begin(9600);

	//Module NRF24
	//============================================================
	RF24_Init();

	//MPU6050
	//============================================================
	MPU6050_Init();

	//RTC
	//============================================================
	if (!rtc.begin())
	{
		Serial.print("Couldn't find RTC");
		while (1)
			;
	}

	if (!rtc.isrunning())
	{
		Serial.print("RTC is NOT running!");
	}

	rtc.adjust(DateTime(F(__DATE__), F(__TIME__))); //auto update from computer time
																									//rtc.adjust(DateTime(2019, 5, 5, 3, 0, 0));// to set the time manualy

	//LoRa
	//============================================================

	if (!LoRa.begin(433E6))
	{
		Serial.println("Starting LoRa failed!");
	}

	//Timer
	//============================================================
	timer_Init();

	//============================================================
	my_packet.seq_num = 0;
	status = 0;

	Serial_Init();
}

void write_To_EEPROM(MPU6050_Info *data, int Or_Address)
{
	EEPROM.write(Or_Address * 15, data->AccXH);
	EEPROM.write(Or_Address * 15 + 1, data->AccXL);
	EEPROM.write(Or_Address * 15 + 2, data->AccYH);
	EEPROM.write(Or_Address * 15 + 3, data->AccYL);
	EEPROM.write(Or_Address * 15 + 4, data->AccZH);
	EEPROM.write(Or_Address * 15 + 5, data->AccZL);

	EEPROM.write(Or_Address * 15 + 6, data->GyrXH);
	EEPROM.write(Or_Address * 15 + 7, data->GyrXL);
	EEPROM.write(Or_Address * 15 + 8, data->GyrYH);
	EEPROM.write(Or_Address * 15 + 9, data->GyrYL);
	EEPROM.write(Or_Address * 15 + 10, data->GyrZH);
	EEPROM.write(Or_Address * 15 + 11, data->GyrZL);

	EEPROM.write(Or_Address * 15 + 12, data->TmpH);
	EEPROM.write(Or_Address * 15 + 13, data->TmpL);
}

char daysOfTheWeek[7][12] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};

//MAINNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
void loop()
{ //Read data from Pedometer
	if (my_Serial.available() > 0)
	{
		Serial.println("");
		my_Serial.write(my_packet.info.AccXH);
	}
	if (status == 1)
	{
		my_packet.seq_num += 1;

		//Serial.print("Request: ");
		//Serial.println(my_packet.seq_num);

		uint32_t age;

		RF24_Write(&my_packet, sizeof(my_packet.seq_num));
		//MPU6050_ReadInfo(&my_packet.info);
		//Serial.print(my_packet.info.AccXH<<8|my_packet.info.AccXL);
		//write_To_EEPROM(&my_packet.info, 0);

		////IOT, ,please comment after end of event
		// Serial.print(my_packet.info.AccXH << 8 | my_packet.info.AccXL);
		// Serial.print(" ");
		// Serial.print(my_packet.info.AccYH << 8 | my_packet.info.AccYL);
		// Serial.print(" ");
		// Serial.print(my_packet.info.AccZH << 8 | my_packet.info.AccZL);

		//uint8_t GyrX = my_packet.info.GyrXH << 8 | my_packet.info.GyrXL;
		//uint8_t GyrY = my_packet.info.GyrYH << 8 | my_packet.info.GyrYL;
		//uint8_t GyrZ = my_packet.info.GyrZH << 8 | my_packet.info.GyrZL;
		if (RF24_Read(&receive_packet, sizeof(receive_packet), 65535))
		{
			//Serial.print("received.AccXH: ");
			if (my_packet.seq_num == receive_packet.seq_num)
			{
				//Serial.print(" ");
				//Serial.println(receive_packet.info.AccXH << 8 | receive_packet.info.AccXL);
				//Serial.println("Same");
				//EEPROM.write(0, my_packet.info.AccXH);
				//Serial.print("mypacket AccH writed: ");
				//Serial.println(my_packet.info.AccXH);

				//Write AccXh received from Pedo to AT at next posision of myPacket.TmpL
				//EEPROM.write(15, receive_packet.info.AccXH);
				//Serial.print("receivedpacket AccXH writed: ");

				//count_Of_packet += 1;

				my_packet.info = receive_packet.info;
				DateTime now = rtc.now();
				my_packet.timestamp = now.unixtime();

				// Serial.print(now.hour());
				// Serial.print(':');
				// Serial.print(now.minute());
				// Serial.print(':');
				// Serial.print(now.second());
				// Serial.print("   ");
				// Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
				// Serial.print(" ,");
				// Serial.print(now.day());
				// Serial.print('/');
				// Serial.print(now.month());
				// Serial.print('/');
				// Serial.print(now.year());
				// Serial.print("\t");

				Serial.print(receive_packet.info.AccXH<<8|receive_packet.info.AccXL);
				Serial.print(" ");
				Serial.print(receive_packet.info.AccYH<<8|receive_packet.info.AccYL);
				Serial.print(" ");
				Serial.println(receive_packet.info.AccZH<<8|receive_packet.info.AccZL);
	

				// Serial.print("flat = ");
				// Serial.println(my_packet.flat);
				// Serial.print("flon = ");
				// Serial.println(my_packet.flon);
				// gps.f_get_position(&my_packet.flat, &my_packet.flon, &age);
				// print_float(my_packet.flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
				// print_float(my_packet.flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);
				// Serial.println();
				// LoRa.beginPacket(true);
				// LoRa.write((byte*) &my_packet, sizeof(my_packet));
				// LoRa.endPacket();
			}
		}
		status = 0;
		//Debug data from AT
		//Serial.print("AccXH read from address 0: ");
		//Serial.println(EEPROM.read(0));
		//Serial.print("AccXh received from address 15: ");
		//Serial.println(EEPROM.read(15));
	}
}
//END MAINNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

ISR(TIMER1_OVF_vect)
{
	//TCNT1 = 53000; //Tang tu 53036 den 65536 ~ 1/20s
	 TCNT1 = 53036/3;
	//TCNT1 = 0;
	status = 1;
}
