#include <printf.h>
#include <RF24_config.h>
#include <RF24.h>
#include <nRF24L01.h>
#include <SPI.h>

#define MPU6050_ADDRESS MPU6050_ADDRESS_AD0_LOW
#include "MPU6050_CONTROLLER.H"

#define RF24_ADDRESS 0xE8E8F0F0E1LL // Address to transfer
#include "RF24_CONTROLLER.H"

struct Packet
{
	word seq_num;
	MPU6050_Info info;
} my_packet;

byte status;

void setup()
{
	Serial.begin(9600);

	//Module NRF24
	//============================================================
	RF24_Init();

	//MPU6050
	//============================================================
	MPU6050_Init();

	//============================================================
	status = 0;
}

void loop()
{
	if (RF24_Read(&my_packet, sizeof(my_packet.seq_num), 65535))
	{
		MPU6050_ReadInfo(&my_packet.info);
		RF24_Write(&my_packet, sizeof(my_packet));

		Serial.print("Request: ");
		Serial.println(my_packet.seq_num);

		Serial.print("my.AccXH: ");
		Serial.println(my_packet.info.AccXH);
		//
		// for(int i=0;i<radio.getPayloadSize();i++)
		// Serial.println(buffer[i]);
		// radio.printDetails();
		// request=0;
	}
}