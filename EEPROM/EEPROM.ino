#define DATAOUT 11     //MOSI
#define DATAIN 12      //MISO
#define SPICLOCK 13    //SPK
#define SLAVESELECT 10 //SLAVESELECT

#define WREN 6
#define WRDI 4
#define RDSR 5
#define WRSR 1
#define READ 3
#define WRITE 2

char buffer[128]; //buffer store data

byte eepromOutputData;
byte eepromInputData;
byte clr;
int address = 0;

void fillBuffer();
char spiTransfer(volatile char data);
byte readEeprom(int EEPROM_Address);

void setup()
{
    Serial.begin(9600);
    pinMode(DATAOUT, OUTPUT);
    pinMode(DATAIN, INPUT);
    pinMode(SPICLOCK, OUTPUT);
    pinMode(SLAVESELECT, OUTPUT);
    digitalWrite(SLAVESELECT, HIGH); //disable device

    //Configure Register
    // SPCR = 01010000
    //interrupt disabled,spi enabled,msb 1st,master,clk low when idle,
    //sample on leading edge of clk,system clock/4 rate (fastest)
    SPCR = (1 << SPE) | (1 << MSTR);
    clr = SPSR; //SPI Status Register
    clr = SPDR; //SPI Data Register
    delay(10);

    //Initialize
    fillBuffer();
    digitalWrite(SLAVESELECT, LOW);  //On device
    spiTransfer(WREN);               //write enable
    digitalWrite(SLAVESELECT, HIGH); //Off device
    delay(10);

    digitalWrite(SLAVESELECT, LOW);
    spiTransfer(WRITE);
    address = 0;
    spiTransfer((char)(address >> 8)); //msb
    spiTransfer((char)(address));      //lsb
    //write 128 byte
    for (int i = 0; i < 128; i++)
    {
        spiTransfer(buffer[i]);
    }
    digitalWrite(SLAVESELECT, HIGH);
    delay(300);

    Serial.print('h');
    Serial.print('i');
    Serial.print('\n');
    delay(1000);
}

void loop()
{
    eepromOutputData = readEeprom(address);
    Serial.print(eepromOutputData, DEC);
    Serial.print('\n');
    address++;
    delay(500);
}

void fillBuffer()
{
    for (int i = 0; i < 128; i++)
    {
        buffer[i] = i;
    }
}

char spiTransfer(volatile char data)
{
    SPDR = data;                  //Start transmition
    while (!(SPSR & (1 << SPIF))) //wait for end transmition //SPI Interrupt Flag
        return SPDR;
}

byte readEeprom(int EEPROM_Address)
{
    int data;
    digitalWrite(SLAVESELECT, LOW);
    spiTransfer(READ);
    spiTransfer((char)(EEPROM_Address >> 8));
    spiTransfer((char)(EEPROM_Address));
    data = spiTransfer(0xFF); //get data byte;
    digitalWrite(SLAVESELECT, HIGH);
    return data;
}
